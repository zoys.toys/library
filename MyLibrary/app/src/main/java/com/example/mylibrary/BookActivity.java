package com.example.mylibrary;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class BookActivity extends AppCompatActivity {

    public static final String BOOK_ID_KEY = "bookId";

    private TextView txtBookname, txtAuthor, txtPages, txtDesc;
    private Button btnAddToWantToRead, btnAddToAlreadyRead, btnAddToCurrentlyReading, btnAddToFavorite;
    private ImageView bookImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        initViews();

//        String longDescription = "adasndjah rh aiudnas rha das jdao hwuao asji as ioawd iow diuaos asddw dioas jd oiaio asd wiuwh uaow" + "\n" +
//            "dwa djwa jdwaiodjwaoijdh fawd sjdiwjd ia dwioadh uibfh iue" + "\n" + "dasd a dwa daswe adgadwuihad dhuiawdha whdiua hdiuawduwia  hduaisdhwu";
//
//        //TODO: Get the data from recycler view in here
//        Book book = new Book(1, "1Q84", "Haruki Murakami", 1350, "https://haikubookreview.files.wordpress.com/2012/07/1q84.jpg",
//                "A work of maddening brilliance", longDescription);

        Intent intent = getIntent();
        if (null != intent) {
            int bookId = intent.getIntExtra(BOOK_ID_KEY, -1);
            if (bookId != -1) {
                Book incomingBook = Utils.getInstance().getBookById(bookId);
                if (null != incomingBook) {
                    setData(incomingBook);

                    handleAlreadyRead(incomingBook);
//                    handleWantToReadBooks(incomingBook);
//                    handleCurrentlyReadingbooks(incomingBook);
//                    handleFavoriteBooks(incomingBook);
                }
            }
        }
    }

//    private void handleFavoriteBooks(final Book book) {
//        ArrayList<Book> FavoriteBooks = Utils.getInstance().getFavoriteBooks();
//
//        boolean existFavoriteBooks = false;
//
//        for (Book b : FavoriteBooks) {
//            if (b.getId() == book.getId()) {
//                existFavoriteBooks = true;
//            }
//        }
//
//        if (existFavoriteBooks) {
//            btnAddToFavorite.setEnabled(false);
//        } else {
//            btnAddToFavorite.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (Utils.getInstance().addFavoriteBooks(book)) {
//                        Toast.makeText(BookActivity.this, "Book Added", Toast.LENGTH_SHORT).show();
//                        Intent intent = new Intent(BookActivity.this, FavoriteBooksActivity.class);
//                        startActivity(intent);
//                    } else {
//                        Toast.makeText(BookActivity.this, "Something wrong happened", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            });
//        }
//    }
//
//    private void handleCurrentlyReadingbooks(final Book book) {
//        ArrayList<Book> ReadingBooks = Utils.getInstance().getCurrentlyReadingBooks();
//
//        boolean existCurrentlyReadingBooks = false;
//
//        for (Book b : ReadingBooks) {
//            if (b.getId() == book.getId()) {
//                existCurrentlyReadingBooks = true;
//            }
//        }
//
//        if (existCurrentlyReadingBooks) {
//            btnAddToCurrentlyReading.setEnabled(false);
//        } else {
//            btnAddToCurrentlyReading.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (Utils.getInstance().addToCurrentlyReadingbooks(book)) {
//                        Toast.makeText(BookActivity.this, "Book Added", Toast.LENGTH_SHORT).show();
//                        Intent intent = new Intent(BookActivity.this, CurrentlyReadingActivity.class);
//                        startActivity(intent);
//                    } else {
//                        Toast.makeText(BookActivity.this, "Something wrong happened", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            });
//        }
//    }
//
//    private void handleWantToReadBooks(final Book book) {
//        ArrayList<Book> wantToReadBooks = Utils.getInstance().getWantToReadBooks();
//
//        boolean existInWantToReadBooks = false;
//
//        for (Book b : wantToReadBooks) {
//            if (b.getId() == book.getId()) {
//                existInWantToReadBooks = true;
//            }
//        }
//
//        if (existInWantToReadBooks) {
//            btnAddToWantToRead.setEnabled(false);
//        } else {
//            btnAddToWantToRead.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (Utils.getInstance().addToWantToRead(book)) {
//                        Toast.makeText(BookActivity.this, "Book Added", Toast.LENGTH_SHORT).show();
//                        Intent intent = new Intent(BookActivity.this, WanToReadActivity.class);
//                        startActivity(intent);
//                    } else {
//                        Toast.makeText(BookActivity.this, "Something wrong happened", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            });
//        }
//    }


    /**
     * Enable and disable button
     * add the book to already read book arraylist
     *
     * @param book
     */

    private void handleAlreadyRead(final Book book) {
        ArrayList<Book> alreadyReadBooks = Utils.getInstance().getAlreadyReadBooks();

        boolean existinAlreadyReadBooks = false;

        for (Book b : alreadyReadBooks) {
            if (b.getId() == book.getId()) {
                existinAlreadyReadBooks = true;
            }
        }

        if (existinAlreadyReadBooks) {
            btnAddToAlreadyRead.setEnabled(false);
        } else {
            btnAddToAlreadyRead.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.getInstance().addToAlreadyRead(book)) {
                        Toast.makeText(BookActivity.this, "Book Added", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(BookActivity.this, AlreadyReadBookActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(BookActivity.this, "Something wrong happened", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void setData(Book book) {
        txtBookname.setText(book.getName());
        txtAuthor.setText(book.getAuthor());
        txtPages.setText(String.valueOf(book.getPages()));
        txtDesc.setText(book.getLongDesc());
        Glide.with(this)
                .asBitmap().load(book.getImageUrl())
                .into(bookImage);
    }


    private void initViews() {
        txtAuthor = findViewById(R.id.txtBookAuthorA);
        txtBookname = findViewById(R.id.txtBookNameB);
        txtPages = findViewById(R.id.txtBookPagesA);
        txtDesc = findViewById(R.id.txtBookLongDesc);

        btnAddToAlreadyRead = findViewById(R.id.btnAddToAlreadyRead);
        btnAddToCurrentlyReading = findViewById(R.id.btnAddToCurrentlyReading);
        btnAddToFavorite = findViewById(R.id.btnAddToFavorites);
        btnAddToWantToRead = findViewById(R.id.btnWantToReadBook);

        bookImage = findViewById(R.id.imageView);
    }

}